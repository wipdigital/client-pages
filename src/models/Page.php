<?php namespace WorkInProgress\ClientPages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Collection;

class Page extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'template_id', 'type_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'active', 'order', 'primary', 'secondary', 'footer'];

  public function pages()
  {
    return $this->hasMany('\WorkInProgress\ClientPages\Page');
  }

  public function template()
  {
    return $this->belongsTo('\WorkInProgress\ClientPages\Template');
  }

  public function sections()
  {
    return $this->hasMany('\WorkInProgress\ClientPages\PageSection');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\ClientPages\PageImage');
  }

  public function parentPage()
  {
    return $this->belongsTo('\WorkInProgress\ClientPages\Page', 'page_id');
  }

  public function getBreadcrumbsAttribute()
  {
    $collection = new Collection;

    for($page = $this->parent_page; isset($page->page_id); $page = $page->parent_page)
    {
      $collection->add($page);
    }

    return $collection->reverse();
  }

  public function getTopParentPageAttribute()
  {
    $page = Page::find($this->attributes['id']);
    while($page->parentPage()->first()->permalink != 'home') {
      $page = $page->parentPage()->first();
    }

    return $page;
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

  public function getThumbnailImageAttribute()
  {
    return $this->images->count() ? $this->images->get(0) : null;
  }

  public function getBackgroundImageAttribute()
  {
    if(!$this->images->count()) {
      return null;
    }

    return ($this->images->count() >=2) ? $this->images->get(1) : $this->images->get(0);
  }

  public function scopePrimary($query)
  {
    return $query->where('active', '=', true)->where('primary', '=', true)->orderBy('order', 'asc');
  }

  public function scopeSecondary($query)
  {
    return $query->where('active', '=', true)->where('secondary', '=', true)->orderBy('order', 'asc');
  }

  public function scopeFooter($query)
  {
    return $query->where('active', '=', true)->where('footer', '=', true)->orderBy('order', 'asc');
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true)->orderBy('order');
  }

  public function getPreviousPageAttribute()
  {
    $previous_page_order = Page::where('page_id', $this->attributes['page_id'])->where('order', '<', $this->attributes['order'])->max('order');

    if($previous_page_order) {
      return Page::where('page_id', $this->attributes['page_id'])->where('order', $previous_page_order)->first();
    }

    return null;
  }

  public function getNextPageAttribute()
  {
    $next_page_order = Page::where('page_id', $this->attributes['page_id'])->where('order', '>', $this->attributes['order'])->min('order');

    if($next_page_order) {
      return Page::where('page_id', $this->attributes['page_id'])->where('order', $next_page_order)->first();
    }

    return null;
  }

}
