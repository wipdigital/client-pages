<?php namespace WorkInProgress\ClientPages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PageSection extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'page_sections';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'src', 'url', 'description', 'featured', 'active', 'order', 'template_id', 'hover_description'];

  public function page()
  {
    return $this->belongsTo('\WorkInProgress\ClientPages\Page');
  }

  public function template()
  {
    return $this->belongsTo('\WorkInProgress\ClientPages\Template');
  }

}

?>
