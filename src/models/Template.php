<?php namespace WorkInProgress\ClientPages;

class Template extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'templates';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

}

?>
