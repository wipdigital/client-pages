<?php namespace WorkInProgress\ClientPages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PageImage extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'page_images';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'template_id', 'src', 'url', 'title', 'short_description', 'featured', 'active', 'order'];

  public function page()
  {
    return $this->belongsTo('\WorkInProgress\ClientPages\Page');
  }

  public function template()
  {
    return $this->hasOne('\WorkInProgress\ClientPages\Template', 'template_id');
  }

  public function getHeightAttribute()
  {
    $size = getimagesize(public_path() . urldecode($this->attributes['src']));

    return count($size) ? $size[1] : null;
  }

  public function getWidthAttribute()
  {

    $size = getimagesize(public_path() . urldecode($this->attributes['src']));

    return count($size) ? $size[0] : null;
  }

}

?>
