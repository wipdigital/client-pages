<?php namespace WorkInProgress\ClientPages;

class PagesController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', array('on' => 'post'));
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getPage($full_permalink = 'home')
  {

    //if no permalink display homepage
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = [
      'page' => $page,
      'pages' => Page::find(1),
      'title' => $page->full_title
    ];

    return \View::make('pages::' . $page->template->src, $data);
	}

}

?>
