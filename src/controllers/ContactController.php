<?php namespace WorkInProgress\ClientPages;

class ContactController extends \BaseController {

	private $rules = [
    'name' => 'required',
    'email' => 'required|email',
    'description' => 'required'
  ];

  public function __construct()
  {
    $this->beforeFilter('csrf', ['on' => 'post']);
  }

  public function postIndex()
  {
    $data = \Input::all();
    $site_email = \DB::table('settings')->where('key', '=', 'site_email')->pluck('value');
    $site_name = \DB::table('settings')->where('key', '=', 'site_name')->pluck('value');
    
    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    $data['site_name'] = $site_name;
    \Mail::send('pages::email.contact.user', $data, function($message) use ($site_name, $site_email, $data) {
        $message->from($site_email, $site_name);
        $message->to($data['email'], $data['name']);
        $message->subject($site_name . ' - Contact Form Enquiry');
    });

    \Mail::send('pages::email.contact.site', $data, function($message) use ($site_name, $site_email, $data) {
        $message->from($data['email'], $data['name']);
        $message->to($site_email);
        $message->subject($site_name . ' - Contact Form Enquiry');
    });

    \Session::flash('message', 'Contact form successfully submitted!');
    return \Redirect::back();
  }

}

?>
