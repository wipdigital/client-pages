@extends('pages::layouts.email')

@section('main')
  <p>Dear {{ $name }},</p>
  <p>This is a confirmation that you have submitted the following contact form enquiry to {{ $site_name}}.</p>

  <h2>Enquiry Details</h2>
  <table>
    <tr><td>Name:</td><td>{{ $name }}</td></tr>
    <tr><td>Email:</td><td>{{ $email }}</td></tr>
  </table>

  <p>{{ $description }}</p>
@stop

