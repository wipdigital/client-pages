@extends('pages::layouts.email')

@section('main')
  <p>Dear {{ $site_name }},</p>
  <p>You have had a contact form enquiry on your website.</p>

  <h2>Enquiry Details</h2>
  <table>
    <tr><td>Name:</td><td>{{ $name }}</td></tr>
    <tr><td>Email:</td><td>{{ $email }}</td></tr>
  </table>

  <p>{{ $description }}</p>
@stop
