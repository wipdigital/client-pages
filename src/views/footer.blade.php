      </main>
      <footer>
        <div class="row collapse form-max">
          <div class="small 12 large-6 columns">
            <div class="social">
              <a href="#" class="fa-stack fa-lg" title="Email Cheep"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-envelope fa-stack-1x"></i></a>
              <a href="#" class="fa-stack fa-lg" title="Cheep on Facebook"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-facebook fa-stack-1x fx"></i></a>
              <a href="#" class="fa-stack fa-lg" title="Cheep on Instagram"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-instagram fa-stack-1x fx"></i></a> 
              <a href="https://twitter.com/" class="fa-stack fa-lg" title="Blue Forest Media on Twitter"><i class="fa fa-circle fa-stack-2x fa-inverse op"></i><i class="fa fa-twitter fa-stack-1x fx"></i></a>
            </div><!--end social-->
          </div>
          <div class="small-12 large-6 columns">
            <div class="large-form">
              <form action="http://wipmail.createsend.com/t/i/s/ijlhjl/" method="post" id="subForm">
                <span class="input-k input--kozakura">
                  <input class="input__field input__field--kozakura btn-space" type="text" id="fieldEmail" name="cm-ijlhjl-ijlhjl"/>
                  <label class="input__label input__label--kozakura" for="fieldEmail">
                    <span class="input__label-content input__label-content--kozakura" data-content="Subscribe"><i class="fa fa-envelope"></i> Subscribe</span>
                  </label>
                  <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                  </svg>
                </span>
                <button type="submit" class="small subscribe" title="Subscribe to Cheep Mailing List"><i class="fa fa-paper-plane"></i></button>
              </form>
            </div>
          </div><!--End Col-->
        </div><!--End Row-->                
        <div class="row collapse">
          <div class="small-12 columns"> 
            <div class="info-nav">
              @include('pages::components.navigation.footer')
            </div>
            <div class="copy">
              <a href="{{ Config::get('app.url') }}" title="">{{ DB::table('settings')->where('key', '=', 'site_name')->pluck('value') }}</a> &copy; {{ date('Y') }}  
            </div>
            <div class="credits">
              website by <a href="http://www.workinprogress.com.au" target="_blank" title="Work In Progress - Website Design, Digital Marketing, Apps &amp; eCommerce in Perth" class="wip"><img src="/images/wip.svg" width="32"></a>
            </div>
          </div><!--End Col-->
        </div><!--End Row-->     
        <div class="clear"></div>
      </footer>
    </div>
  </div>
  <nav class="outer-nav left vertical">
    @include('pages::components.navigation.perspective')
  </nav>
</div>
