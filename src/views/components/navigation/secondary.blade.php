@if(count($page->top_parent_page->pages()->secondary()->get()))
<ul class="side-nav">
  @foreach($page->top_parent_page->pages()->secondary()->get() as $p)
    @include('pages::components.navigation._secondary', $p)
  @endforeach
</ul>
@endif
