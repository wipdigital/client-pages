<li class=" @if(count($p->pages()->secondary()->get())) has-dropdown @endif @if($page->id == $p->id) active @endif"><a href="/{{ $p->full_permalink }}" title="{{ $p->short_description }}">{{ $p->title }}</a>

  @if(count($p->pages()->secondary()->get()))
    <ul class="dropdown">
    @foreach($p->pages()->secondary()->get() as $p)
      @include('pages::components.navigation._secondary', $p)
    @endforeach
    </ul>
  @endif
</li>
