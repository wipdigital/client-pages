<a @if($page->id == 1) class="active" @endif href="/" data-hover="Home">Home</a>
@foreach($pages->pages()->primary()->get() as $p)
  <a @if($page->id == $p->id) class="active" @endif href="/{{ $p->full_permalink }}" title="{{ $p->short_description }}">{{ $p->title }}</a>
@endforeach
