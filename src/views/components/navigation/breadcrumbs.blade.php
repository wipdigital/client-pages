<div class="navigation__breadcrumbs">
  <nav class="breadcrumbs" role="menubar" aria-label="breadcrumbs">
    <a href="/">Home</a>
    @foreach($resource->breadcrumbs as $page)
    <a href="/{{ $page->full_permalink }}">{{ $page->title}}</a>
    @endforeach
    <a href="/{{ $resource->full_permalink }}" class="current">{{ $resource->title }}</a>
  </nav>
</div>
