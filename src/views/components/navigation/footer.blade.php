@foreach($pages->pages()->footer()->get() as $p)
  <a href="/{{ $p->full_permalink }}" title="{{ $p->short_description }}" class="@if($page->id == $p->id) active @endif" title="{{ $p->short_description }}">{{ $p->title }}</a>
@endforeach
