<div class="navigation__primary fixed">
  <nav class="top-bar" role="navigation">
    <ul class="title-area">
      <div class="nav-activator hidden-for-medium-up">
        <button type="button" role="button" aria-label="Toggle Navigation" class="lines-button x fadeIn slow-fade" id="showMenu">
          <span class="lines"></span>
        </button>
      </div>    
      
      <li class="name">
        <a href="/" title="{{ DB::table('settings')->where('key', '=', 'site_name')->pluck('value') }}">
          @include('pages::components.logo')
        </a>  
      </li>
    </ul>

    <section class="top-bar-section nv-lnks">
      <ul class="right hide-for-small-only">
        <li @if($page->id == 1) class="active"@endif><a href="/" data-hover="Home" title="Home">Home</a></li>
        @foreach($pages->pages()->primary()->get() as $p)
          @include('pages::components.navigation._primary', $p)
        @endforeach
      </ul>
    </section>    
  </nav>
</div>
