<li class=" @if(count($p->pages()->primary()->get())) has-dropdown @endif @if($page->id == $p->id) active @endif">
  <a href="/{{ $p->full_permalink }}" title="{{ $p->short_description }}" data-hover="{{ $p->title }}">{{ $p->title }}</a>

  @if(count($p->pages()->primary()->get()))
    <ul class="dropdown">
    @foreach($p->pages()->primary()->get() as $p)
      @include('pages::components.navigation._primary', $p)
    @endforeach
    </ul>
  @endif
</li>
