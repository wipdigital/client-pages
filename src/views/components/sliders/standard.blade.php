@if(count($images))
<div class="slider__standard">
  <div class="cycle-slideshow" data-cycle-fx="fadeout" data-cycle-pause-on-hover="true" data-cycle-speed="1000" data-cycle-easing="swing" data-cycle-overlay-template="<div><p class='title'>@{{title}}</p> <p>@{{desc}}<br><span>VIEW <strong>NOW</strong></span></p></div>" data-cycle-slides="> .cycle-slide">
    <div class="cycle-overlay"></div>

    @foreach($images as $image)
    <div class="cycle-slide"@if($image->title) data-title="{{ $image->title }}"@endif @if($image->short_description) data-desc="{{ $image->short_description }}"@endif>
      @if($image->url)<a href="{{ $image->url }}">@endif
      <img src="{{ Config::get('ecommerce::product.cdn') . $image->src }}" class="wow zoomIn">
      @if($image->url)</a>@endif
    </div>
    @endforeach
  </div>
</div>
@endif

