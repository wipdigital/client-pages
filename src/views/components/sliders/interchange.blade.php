@if(count($images))
  <div class="show-for-large-up slider__interchange">
    <div class="cycle-slideshow" data-cycle-fx="fadeout" data-cycle-pause-on-hover="true" data-cycle-speed="1000" data-cycle-easing="swing" data-cycle-overlay-template="<div><p class='title'>@{{title}}</p> <p>@{{desc}}<br><span>VIEW <strong>NOW</strong></span></p></div>" data-cycle-slides="> .cycle-slide">
      <div class="cycle-overlay"></div>
      @foreach($images as $image)
      <div class="cycle-slide"@if($image->title) data-title="{{ $image->title }}"@endif @if($image->short_description) data-desc="{{ $image->short_description }}"@endif>
        @if($image->url)<a href="{{ $image->url }}">@endif
        <img src="{{ Config::get('ecommerce::product.cdn') . $image->src }}" class="animated zoomIn">
        @if($image->url)</a>@endif
      </div>
      @endforeach
    </div>
  </div>

  <div class="hide-for-large-up slider__interchange">
    <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-swipe=true data-cycle-swipe-fx="scrollHorz" data-cycle-speed="800" data-cycle-timeout="6000" data-cycle-easing="swing" data-cycle-overlay-template="<div>@{{title}}</div>" data-cycle-slides="> .cycle-slide" data-cycle-overlay="> .cycle-overlay">
      <div class="cycle-overlay"></div>
      @foreach($images as $image)
      <div class="cycle-slide"@if($image->title) data-title="{{ $image->title }}"@endif>
        @if($image->url)<a href="{{ $image->url }}">@endif
        <img src="{{ Config::get('ecommerce::product.cdn') . $image->src }}" class="animated zoomIn">
        @if($image->url)</a>@endif
      </div>
      @endforeach
    </div>
  </div>
@endif

<script>
$(function() {
  var windowResize = function() {
    if($(window).width() >= 1024) {
      largeActive = true;
      $('.slider__interchange.show-for-large-up > .cycle-slideshow').cycle('resume');
      $('.slider__interchange.hide-for-large-up > .cycle-slideshow').cycle('pause');
    } else if($(window).width() < 1024) {
      largeActive = false;
      $('.slider__interchange.show-for-large-up > .cycle-slideshow').cycle('pause');
      $('.slider__interchange.hide-for-large-up > .cycle-slideshow').cycle('resume');
    }
  }

  $(window).on('resize', windowResize);
  windowResize();
});
</script>
