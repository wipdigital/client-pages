@extends('layouts.master')

@section('layout')
  @include('pages::header')

  @yield('main')

  @include('pages::footer')
@stop

