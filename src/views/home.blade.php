@extends('pages::layouts.standard')

@section('main')

  @include('pages::components.sliders.standard', ['images' => $page->images])

  {{ $page->description }}

@stop
