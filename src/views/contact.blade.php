@extends('layouts.master')

@section('layout')
  @include('pages::header')

  @include('pages::components.sliders.internal', ['images' => $page->images])

  <div class="row"> 
    <div class="columns medium-6">
    {{ $page->description }}
    </div>
    <div class="columns medium-6">
      {{ Form::open(['url' => '/']) }}
        <!--Name-->
        <span class="input-k input--kozakura inner @if(Input::old('name')) input--filled @endif">
          {{ Form::text('name', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'name']) }}
          <label class="input__label input__label--kozakura inner" for="name">
            <span class="input__label-content input__label-content--kozakura" data-content="Name"><i class="fa fa-user"></i> Name</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('email'))
        <div class="alert-box">{{ $errors->first('email') }}</div>
        @endif

        <!--Email-->
        <span class="input-k input--kozakura inner @if(Input::old('email')) input--filled @endif">
          {{ Form::email('email', null, ['class' => 'input__field input__field--kozakura inner', 'id' => 'form-email']) }}
          <label class="input__label input__label--kozakura inner" for="form-email">
            <span class="input__label-content input__label-content--kozakura" data-content="Email"><i class="fa fa-envelope"></i> Email</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('email'))
        <div class="alert-box">{{ $errors->first('email') }}</div>
        @endif

        <!--Message-->
       <span class="input-k input--kozakura textarea-control inner @if(Input::old('description')) input--filled @endif">
          <textarea class="input__field input__field--kozakura textarea inner" type="textarea" id="message" name="description" /></textarea>
          <label class="input__label input__label--kozakura inner" for="message">
            <span class="input__label-content input__label-content--kozakura" data-content="Message"><i class="fa fa-comment"></i> Message</span>
          </label>
          <svg class="graphic graphic--kozakura inner" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
          </svg>
        </span>
        @if($errors->has('description'))
        <div class="alert-box">{{ $errors->first('description') }}</div>
        @endif

        {{ Form::submit('Submit', ['class' => 'button zfx']) }}
      {{ Form::close() }}
    </div>
  </div>
  @include('pages::footer')
@stop
