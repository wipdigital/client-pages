@extends('pages::layouts.standard')

@section('main')

  @include('pages::components.sliders.internal', ['images' => $page->images])

  @include('pages::components.navigation.breadcrumbs', ['resource' => $page])

  <div class="row">
    <div class="medium-3 columns">
      @include('pages::components.navigation.secondary')
    </div>
    <div class="medium-9 columns">

      {{ $page->description }}
    </div>
  </div>
@stop
