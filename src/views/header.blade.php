<header>
  @include('pages::components.navigation.primary')
</header>
<div id="perspective" class="perspective effect-airbnb">
  <div class="container">
    <div class="wrapper">
      <main>
        <header class="fake-header">
          <nav class="top-bar" role="navigation">
            <ul class="title-area">
              <div class="nav-activator hidden-for-medium-up">
                <button type="button" role="button" aria-label="Toggle Navigation" class="lines-button x fadeIn slow-fade">
                  <span class="lines"></span>
                </button>
              </div>
              <li class="name">
                <a href="/" title="{{ DB::table('settings')->where('key', '=', 'site_name')->pluck('value') }}">
                  @include('pages::components.logo')
                </a>  
              </li>
            </ul>
          </nav>
        </header>
