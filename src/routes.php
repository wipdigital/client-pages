<?php

Route::get('/', 'WorkInProgress\ClientPages\PagesController@getPage');
Route::post('/', 'WorkInProgress\ClientPages\ContactController@postIndex');
Route::get('/{full_permalink}', 'WorkInProgress\ClientPages\PagesController@getPage')->where('full_permalink', '(.*)');

?>
